# -*- coding: utf-8 -*-
"""ex2_3.ipynb

ex2_3 自適應二值化

程式： OmniXRI Jack 2020.09.05
"""

#!wget -N https://gitlab.com/omnixri/tcn20200905/-/raw/master/samples/Lena.png
#!ls

import cv2
import numpy as np
import matplotlib.pyplot as plt

img = cv2.imread('Lena.png',cv2.IMREAD_GRAYSCALE) #讀入影像轉成灰階
ret,th1 = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
th2 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_MEAN_C, \
cv2.THRESH_BINARY,11,2)
th3 = cv2.adaptiveThreshold(img,255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, \
cv2.THRESH_BINARY,11,2)
titles = ['img','BINARY','MEAN_C','GAUSSIAN_C']
images = [img,th1,th2,th3]
for i in range(4): # 繪製結果影像
  plt.subplot(2,2,i+1),plt.imshow(images[i],'gray')
  plt.title(titles[i])
  plt.xticks([]),plt.yticks([])
plt.show()