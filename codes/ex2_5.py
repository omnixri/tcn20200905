# -*- coding: utf-8 -*-
"""ex2_5.ipynb

ex2_5 色彩空間轉換

程式： OmniXRI Jack 2020.09.05
"""

!wget -N https://gitlab.com/omnixri/tcn20200905/-/raw/master/samples/Lena.png
!ls

import cv2 # 引入OpenCV函式庫

# 讀入彩色影像
img1 = cv2.imread('Lena.png')
# 將彩色影像轉換成指定色彩空間影像
img2 = cv2.cvtColor(img1,cv2.XXXXXX)
# 顯示影像img2，視窗標題My Image 
cv2.imshow(‘My Image’,img2)
# 將新影像img2寫入磁碟，指定檔名及格式
cv2.imwrite('輸出灰階影像名稱',img2)
cv2.waitKey(0) # 等待按鍵結束程式
cv2.destroyAllWindows() # 關閉所有視窗