# -*- coding: utf-8 -*-
"""ex3_1.ipynb

ex3_1 邊緣偵測

程式： OmniXRI Jack 2020.09.05
"""

#!wget -N https://gitlab.com/omnixri/tcn20200905/-/raw/master/samples/Lena.png
#!ls

# 導入必要函式庫
import cv2
import numpy as np
import matplotlib.pyplot as plt

imgG = cv2.imread('Lena.png', cv2.IMREAD_GRAYSCALE) # 讀入彩色影像並轉成灰階
imgLap = cv2.Laplacian(imgG,cv2.CV_16S,ksize=3)
imgL = cv2.convertScaleAbs(imgLap) # 將int16S轉回uint8
imgC = cv2.Canny(imgG,30,150) # 起點閾值,結束閾值
imgX = cv2.Sobel(imgG,cv2.CV_16S,1,0) # 計算Sobel X方向邊緣
imgY = cv2.Sobel(imgG,cv2.CV_16S,0,1) # 計算Sobel Y方向邊緣
absX = cv2.convertScaleAbs(imgX) # 將int16S轉回uint8
absY = cv2.convertScaleAbs(imgY) # 將int16S轉回uint8
imgXY = cv2.addWeighted(absX,0.5,absY,0.5,0) # imgXY=0.5*absX+0.5*absY+0
titles = ['img','Laplacian','Canny','Sobel x','Sobel y','sobel xy']
images = [imgG,imgL,imgC,absX,absY,imgXY]
for i in range(6): # 繪製結果影像
  plt.subplot(2,3,i+1),plt.imshow(images[i],'gray')
  plt.title(titles[i])
  plt.xticks([]),plt.yticks([])
plt.show()