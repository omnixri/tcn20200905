# -*- coding: utf-8 -*-
"""ex3_3.ipynb

ex3_3 影像銳化

程式： OmniXRI Jack 2020.09.05
"""

#!wget -N https://gitlab.com/omnixri/tcn20200905/-/raw/master/samples/Lena.png
#!ls

# 導入必要函式庫
import cv2
import numpy as np
import matplotlib.pyplot as plt

imgGray = cv2.imread('Lena.png', cv2.IMREAD_GRAYSCALE) # 讀入彩色影像並轉成灰階
kernel = np.array((
    [-1, -1, -1],
    [-1, 9, -1],
    [-1, -1, -1]), dtype="float32") # 建立3*3卷積核，可銳化可平滑化
imgS = cv2.filter2D(imgGray,-1,kernel) # 通用型濾波器
titles = ['img','sharp']
images = [imgGray,imgS]
for i in range(2): # 繪製結果影像
  plt.subplot(1,3,i+1),plt.imshow(images[i],'gray')
  plt.title(titles[i])
  plt.xticks([]),plt.yticks([])
plt.rcParams['savefig.dpi'] = 300 # 設定圖片尺寸 
plt.rcParams['figure.dpi'] = 300 # 設定圖片解析度
plt.show()