# -*- coding: utf-8 -*-
"""ex2_4.ipynb

ex2_4 亮度/對比調整

程式： OmniXRI Jack 2020.09.05
"""

#!wget -N https://gitlab.com/omnixri/tcn20200905/-/raw/master/samples/Lena.png
#!ls

import cv2
import numpy as np
import matplotlib.pyplot as plt

imgSrc = cv2.imread('Lena.png',cv2.IMREAD_COLOR)
c = 1.3 # 放大量，調整對比
b = -50 # 偏置量，調整亮度
#創建一尺寸和輸入影像大小相同的全零(黑)影像
blank = np.zeros(imgSrc.shape,imgSrc.dtype)
imgAdj = cv2.addWeighted(imgSrc,c,blank,1-c,b) # 加權計算
rgb1 = cv2.cvtColor(imgSrc,cv2.COLOR_BGR2RGB) # BGR轉RGB
rgb2 = cv2.cvtColor(imgAdj,cv2.COLOR_BGR2RGB) # BGR轉RGB
titles = ['img','Adj.']
images = [rgb1,rgb2]
for i in range(2): # 繪製結果影像
  plt.subplot(1,2,i+1),plt.imshow(images[i])
  plt.title(titles[i])
  plt.xticks([]),plt.yticks([])
plt.show()