# -*- coding: utf-8 -*-
"""ex4_3.ipynb

ex4_3 圖文標示

程式： OmniXRI Jack 2020.09.05
"""

#!wget -N https://gitlab.com/omnixri/tcn20200905/-/raw/master/samples/Lena.png
#!ls

# 導入必要函式庫
import cv2
import numpy as np
import matplotlib.pyplot as plt

img = cv2.imread('Lena.png') # 讀入彩色影像

font = cv2.FONT_HERSHEY_SIMPLEX # 定義字體
text = 'OpenCV' # 顯示文字
f_pos = (50,50) # 文字位置 (x,y)
f_size = 1.2 # 字型大小（倍率）
f_color = (255,0,0) # 文字色彩 B,G,R
line_size = 2 # 文字線粗

img2 = cv2.putText(img, text, f_pos, font, f_size, f_color, line_size) # 在影像上放上文字
rgb = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB) # 將影像從BGR轉成RGB格式
plt.imshow(rgb) # 顯示結果影像
plt.show()