# -*- coding: utf-8 -*-
"""ex3_2.ipynb

ex3_2 影像平滑化

程式： OmniXRI Jack 2020.09.05
"""

#!wget -N https://gitlab.com/omnixri/tcn20200905/-/raw/master/samples/Lena.png
#!ls

# 導入必要函式庫
import cv2
import numpy as np
import matplotlib.pyplot as plt

imgGray = cv2.imread('Lena.png', cv2.IMREAD_GRAYSCALE) # 讀入彩色影像並轉成灰階
imgBlur = cv2.blur(imgGray,(3,3)) # 均值濾波，核大小3*3
imgGau = cv2.GaussianBlur(imgGray,(11,11),0) # 高斯濾波，核大小11*11
imgMed = cv2.medianBlur(imgGray,5) # 中值（排序）濾波，核大小5*5
imgBi = cv2.bilateralFilter(imgGray,9,75,75) # 雙邊（保邊）濾波，直徑，空間及灰階標準差
titles = ['img','Blur','Gaussian','Median','Bilateral']
images = [imgGray,imgBlur,imgGau,imgMed,imgBi]
for i in range(5): # 繪製結果影像
  plt.subplot(2,3,i+1),plt.imshow(images[i],'gray')
  plt.title(titles[i])
  plt.xticks([]),plt.yticks([])
plt.rcParams['savefig.dpi'] = 300 # 設定圖片尺寸 
plt.rcParams['figure.dpi'] = 300 # 設定圖片解析度
plt.show()