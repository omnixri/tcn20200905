# -*- coding: utf-8 -*-
"""ex2_6.ipynb

ex2_6 彩色影像分離

程式： OmniXRI Jack 2020.09.05
"""

#!wget -N https://gitlab.com/omnixri/tcn20200905/-/raw/master/samples/color_ball.jpg
#!ls

import cv2
import numpy as np
import matplotlib.pyplot as plt

imgSrc = cv2.imread('color_ball.jpg',cv2.IMREAD_COLOR) # 讀入彩色影像
imgB,imgG,imgR = cv2.split(imgSrc) # 分離BGR彩色影像到三個獨立影像
imgYCrCb = cv2.cvtColor(imgSrc,cv2.COLOR_BGR2YCrCb) # 轉換BGR彩色影像至YCrCb格式
imgY,imgCr,imgCb = cv2.split(imgYCrCb) # 分離YCrCb彩色影像到三個獨立影像
rgb1 = cv2.cvtColor(imgSrc,cv2.COLOR_BGR2RGB)
titles = ['BGR','B','G','R','RGB','Y','Cr','Cb']
images = [imgSrc,imgB,imgG,imgR,rgb1,imgY,imgCr,imgCb]
for i in range(8): # 繪製結果影像
  if i==0 or i==4: # 若為第0, 4組則繪製彩圖
    plt.subplot(2,4,i+1),plt.imshow(images[i])
  else: # 其餘繪製灰階圖
    plt.subplot(2,4,i+1),plt.imshow(images[i],'gray')
  plt.title(titles[i])
  plt.xticks([]),plt.yticks([])
plt.show()