# -*- coding: utf-8 -*-
"""ex4_1.ipynb

ex4_1 人臉偵測

程式： OmniXRI Jack 2020.09.05
"""

#!wget -N https://gitlab.com/omnixri/tcn20200905/-/raw/master/samples/Lena.png
#!wget -N https://gitlab.com/omnixri/tcn20200905/-/raw/master/samples/haarcascade_frontalface_default.xml
#!wget -N https://gitlab.com/omnixri/tcn20200905/-/raw/master/samples/haarcascade_eye.xml
#!ls

# 導入必要函式庫
import cv2
import numpy as np
import matplotlib.pyplot as plt

# 宣告正臉及眼睛的聯級分類器
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')
img = cv2.imread('Lena.png') # 讀入彩色影像
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) # 轉成灰階影像
faces = face_cascade.detectMultiScale(gray, 1.3, 5) # 偵測人臉
# 繪製人臉外框並以人臉區域影像再偵測人眼外框
for (x,y,w,h) in faces:
    img = cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2) # 繪藍框
    roi_gray = gray[y:y+h, x:x+w] # 取得人臉位置局部灰階影像
    roi_color = img[y:y+h, x:x+w] # 取得人臉位置局部彩色影像
    eyes = eye_cascade.detectMultiScale(roi_gray) # 偵測眼睛
    for (ex,ey,ew,eh) in eyes:
        cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2) #繪綠框

rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB) # 將影像從BGR轉成RGB格式
plt.imshow(rgb) # 顯示結果影像
plt.show()