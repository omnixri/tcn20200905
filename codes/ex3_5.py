# -*- coding: utf-8 -*-
"""ex3_5.ipynb

ex3_5 直方圖等化

程式： OmniXRI Jack 2020.09.05
"""

#!wget -N https://gitlab.com/omnixri/tcn20200905/-/raw/master/samples/Lena.png
#!ls

# 導入必要函式庫
import cv2
import numpy as np
import matplotlib.pyplot as plt

imgC = cv2.imread('Lena.png') # 讀入彩色影像
imgYCrCb = cv2.cvtColor(imgC,cv2.COLOR_BGR2YCrCb) # 將彩色影像從BGR轉到YCrCb色彩空間
imgY,imgCr,imgCb = cv2.split(imgYCrCb) # 分離YCrCb三通道影像到三個單通道影像
imgYE = cv2.equalizeHist(imgY) # 對亮度影像imgY進行直方圖等化
imgM = cv2.merge([imgYE,imgCr,imgCb]) # 重新合併imgYE,imgCr,imgCb
rgb1 = cv2.cvtColor(imgC,cv2.COLOR_BGR2RGB) # 將BGR格式轉成RGB格式
rgb2 = cv2.cvtColor(imgM,cv2.COLOR_YCrCb2RGB) #將YCrCb格式轉成RGB格式
titles = ['img','gray','enhance','equal']
images = [rgb1,imgY,rgb2,imgYE]
for i in range(4): # 繪製結果影像
  if i==0 or i==2:
    plt.subplot(2,2,i+1),plt.imshow(images[i])
  else:
    plt.subplot(2,2,i+1),plt.imshow(images[i],'gray')

  plt.title(titles[i])
  plt.xticks([]),plt.yticks([])
plt.rcParams['savefig.dpi'] = 300 # 設定圖片尺寸 
plt.rcParams['figure.dpi'] = 300 # 設定圖片解析度
plt.show()