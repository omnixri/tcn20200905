# -*- coding: utf-8 -*-
"""ex3_7.ipynb

ex3_7 繪圖函數測試

程式： OmniXRI Jack 2020.09.05
"""

# 導入必要函式庫
import cv2
import numpy as np
import matplotlib.pyplot as plt

canvas = np.zeros((200,200,3),dtype="uint8") # 產生一空白（全黑）的影像
color_r = (0,0,255) # 設定紅色
color_g = (0,255,0) # 設定綠色
color_b = (255,0,0) # 設定藍色
cv2.line(canvas,(0,0),(200,200),color_b,2) # 繪製一藍色線，左上到右下，線寬2px
cv2.rectangle(canvas,(50,50),(100,80),color_g,2) # 繪製一個綠色空心矩形，線寬2px
cv2.rectangle(canvas,(150,50),(180,120),color_r,-1) # 繪製一個紅色實心矩形
cv2.circle(canvas,(100,100),80,color_g,2) # 繪製一綠色空心圓形，半徑80，線寬2px
# 繪製一紅色空心橢圓中心(50,150)，軸長(25,45)，傾斜45度，從0度畫到360度，線寬2px
cv2.ellipse(canvas,(50,150),(25,45),45,0,360,color_r,2)
# 繪製綠色文字'OpenCV'到(50,150)，字形FONT_HERSHEY_SIMPLEX，字0.8倍大小，線寬1px 
cv2.putText(canvas,'OpenCV',(50,150),cv2.FONT_HERSHEY_SIMPLEX,0.8,color_g,1) 
rgb = cv2.cvtColor(canvas,cv2.COLOR_BGR2RGB) # 將BGR格式轉成RGB格式
plt.imshow(rgb)
plt.show()