# -*- coding: utf-8 -*-
"""ex2_7.ipynb

OpenCV+Python範例2-7：找出藍色球位置

程式：OmniXRI Jack 2020.9.5
"""

# 下載色球影像
#!wget -N https://gitlab.com/omnixri/tcn20200905/-/raw/master/samples/color_ball.jpg

# 導入必要函式庫
import cv2
import numpy as np
import matplotlib.pyplot as plt

imgSrc = cv2.imread('color_ball.jpg',cv2.IMREAD_COLOR) # 讀取彩色影像檔案
imgHSV = cv2.cvtColor(imgSrc, cv2.COLOR_BGR2HSV) # 將BGR色彩空間轉換到HSV
H,S,V = cv2.split(imgHSV) # 色相H為0到179，代表0到359度, 飽和度S,亮度V皆為0到255
Lower = np.array([100, 100, 50]) # 設定藍色H,S,V下限
Upper = np.array([130, 255, 255]) # 設定藍色H,S,V上限
mask = cv2.inRange(imgHSV, Lower, Upper) # 產生範圍遮罩
BlueThings = cv2.bitwise_and(imgSrc, imgSrc, mask=mask) # 和遮罩進行AND邏輯運算過濾出藍色區域

rgb1 = cv2.cvtColor(imgSrc,cv2.COLOR_BGR2RGB) # 將BGR格式轉成RGB格式
rgb2 = cv2.cvtColor(BlueThings,cv2.COLOR_BGR2RGB) # 將BGR格式轉成RGB格式
titles = ['img','filter'] # 設定顯示標題
images = [rgb1,rgb2] # 設定顯示內容

for i in range(2): # 顯示結果影像
    plt.subplot(1,2,i+1),plt.imshow(images[i])
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])
plt.show() # 繪製顯示內容