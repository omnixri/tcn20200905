# -*- coding: utf-8 -*-
"""ex2_1.ipynb

ex2_1 讀影像和顯示屬性

程式： OmniXRI Jack 2020.09.05
"""

#!wget -N https://gitlab.com/omnixri/tcn20200905/-/raw/master/samples/Lena.png
#!ls

import cv2 # 引入OpenCV函式庫
import numpy as np
import matplotlib.pyplot as plt

# 讀入影像並直接轉成灰階影像
img_gray = cv2.imread('Lena.png',cv2.IMREAD_GRAYSCALE)
print(img_gray.shape) # 顯示(高, 寬, 通道數)
print(img_gray.size) # 顯示影像大小(高*寬*通道數）
print(img_gray.dtype) # 顯示影像格式(uint8)
plt.imshow(img_gray,'gray') # 顯示影像
plt.show()