# -*- coding: utf-8 -*-
"""ex3_6.ipynb

ex3_6 影像縮放/翻轉

程式： OmniXRI Jack 2020.09.05
"""

#!wget -N https://gitlab.com/omnixri/tcn20200905/-/raw/master/samples/Lena.png
#!ls

# 導入必要函式庫
import cv2
import numpy as np
import matplotlib.pyplot as plt

imgC = cv2.imread('Lena.png') # 讀入彩色影像
imgR = cv2.resize(imgC,(256,256),interpolation=cv2.INTER_CUBIC) # 插值方式INTER_CUBIC
imgH = cv2.flip(imgR,1) # 影像水平翻轉
imgV = cv2.flip(imgR,0) # 影像垂直翻轉
imgHV = cv2.flip(imgR,-1) # 影像水平垂直翻轉
titles = ['img','flip-H','flip-V','flip-HV']
images = [imgR,imgH,imgV,imgHV]
for i in range(4): # 繪製結果影像
  rgb = cv2.cvtColor(images[i],cv2.COLOR_BGR2RGB) # 將BGR格式影像轉換成RGB格式
  plt.subplot(2,2,i+1),plt.imshow(rgb)
  plt.title(titles[i])
  plt.xticks([]),plt.yticks([])
plt.rcParams['savefig.dpi'] = 300 # 設定圖片尺寸 
plt.rcParams['figure.dpi'] = 300 # 設定圖片解析度
plt.show()