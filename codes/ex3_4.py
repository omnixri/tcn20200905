# -*- coding: utf-8 -*-
"""ex3_4.ipynb

ex3_4 伽馬校正

程式： OmniXRI Jack 2020.09.05

說明： 只支援灰階影像
"""

#!wget -N https://gitlab.com/omnixri/tcn20200905/-/raw/master/samples/Lena.png
#!ls

# 導入必要函式庫
import cv2
import numpy as np
import matplotlib.pyplot as plt

imgGray = cv2.imread('Lena.png', cv2.IMREAD_GRAYSCALE) # 讀入彩色影像並轉成灰階
imgG05 = np.power(imgGray/float(np.max(imgGray)),0.5) # gamma = 0.5
imgG15 = np.power(imgGray/float(np.max(imgGray)),1.5) # gamma = 1.5
titles = ['img','gamma=0.5','gamma=1.5']
images = [imgGray,imgG05,imgG15]
for i in range(3): # 繪製結果影像
  plt.subplot(1,3,i+1),plt.imshow(images[i],'gray')
  plt.title(titles[i])
  plt.xticks([]),plt.yticks([])
plt.rcParams['savefig.dpi'] = 300 # 設定圖片尺寸 
plt.rcParams['figure.dpi'] = 300 # 設定圖片解析度
plt.show()