# -*- coding: utf-8 -*-
"""ex4_4.ipynb

ex4_4 影像裁切

程式： OmniXRI Jack 2020.09.05
"""

#!wget -N https://gitlab.com/omnixri/tcn20200905/-/raw/master/samples/Lena.png
#!ls

# 導入必要函式庫
import cv2
import numpy as np
import matplotlib.pyplot as plt

img = cv2.imread('Lena.png') # 讀入彩色影像
x,y,w,h = (220,200,140,190)
roi = img[y:y+h, x:x+w] # 取得人臉位置局部彩色影像
rgb1 = cv2.cvtColor(img, cv2.COLOR_BGR2RGB) # 將影像從BGR轉成RGB格式
rgb2 = cv2.cvtColor(roi, cv2.COLOR_BGR2RGB) # 將影像從BGR轉成RGB格式
titles = ['img','ROI']
images = [rgb1,rgb2]
for i in range(2): # 繪製結果影像
    plt.subplot(1,2,i+1),plt.imshow(images[i])
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])
plt.show()